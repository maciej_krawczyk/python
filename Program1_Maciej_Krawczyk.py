# Definiowanie danych wejściowych
n = 3
m = 16

# Funkcja realizująca wyświetlanie danych zgodnie z założeniami 
def FizzBuzz(n,m): 
    if n > 1 & m <=10000 & n < m:
        for num in range(n, m+1):
            if num % 3 == 0 and num % 5 == 0:
                print 'FizzBuzz'    
            elif num % 3 == 0:
                print 'Fizz' 
            elif num % 5 == 0:
                print 'Buzz'
            else:
                print num
# Wyświetlanie danych 
FizzBuzz(n,m)