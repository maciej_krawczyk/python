from __future__ import division
import dobre

def test_average():
    tot = dobre.average((5,2))
    tot1 = dobre.average((800,20))
    tot2 = dobre.average((500,100))
    assert tot == 3.5, tot1 == 40
    assert tot2 == 300
                           
def test_ratio_m():
    tot3 = dobre.ratio_m(range(0,2))
    tot4 = dobre.ratio_m(range(0,5))
    tot5 = dobre.ratio_m(range(0,9))
    assert tot3 == [2.4, 2.1]
    assert tot4 == [2.4, 2.1, 1, 2.1, 2.1]
    assert tot5 == [2.4, 2.1, 1, 2.1, 2.1, 1, 2.4, 2.1, 2.4]
        
def test_to():
    c_, c_1, c_2, top_priced_count_ = dobre.to(range(0,5))
    assert c_ == 2
    assert c_1 == 1
    assert c_2 == 2
    assert top_priced_count_ == [3, 2, 2, 2, 3]

def test_to():
    c, c1, c2, top_priced_count = dobre.to(range(0,9))
    assert c == 3
    assert c1 == 2
    assert c2 == 4
    assert top_priced_count == [3, 2, 2, 2, 3, 2, 3, 3, 2]