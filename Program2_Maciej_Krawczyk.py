# Import bibliotek, danych i definiowanie zmiennych 
from __future__ import division
import pandas as pd
import numpy as np

data = pd.read_excel('data.xlsx')
currencies = pd.read_excel('currencies.xlsx')
matchings = pd.read_excel('matchings.xlsx')
l = range(0,len(data['id'] + 1))
n = matchings['top_priced_count'][0]
n1 = matchings['top_priced_count'][1]
n2 = matchings['top_priced_count'][2]

# Stworzenie wektora ratio 
def ratio_m(l):
    ratio = [0] * len(l)
    for num in l:
        if data['currency'][num] == 'GBP':
            ratio[num] = 2.4
        elif data['currency'][num] == 'EU':
            ratio[num] = 2.1
        elif data['currency'][num] == 'PLN':
            ratio[num] = 1
    return ratio
            
# Stworzenie wektora top_priced_count    
def to(l):
    c = c1 = c2 = 0
    top_priced_count = [0] * len(l)
    for top in l:
        if data['matching_id'][top] == 1:
            top_priced_count[top] = 2
            c += 1
        elif data['matching_id'][top] == 2:
            top_priced_count[top] = 2
            c1 +=1
        elif data['matching_id'][top] == 3:
            top_priced_count[top] = 3     
            c2 += 1
    return c, c1, c2, top_priced_count
            
# Wywołanie funkcji 
ratio = ratio_m(l)
c, c1, c2, top_priced_count = to(l)

# Stworzenie listy z danymi
top_products = pd.DataFrame(np.zeros((len(data),5)),index='1 2 3 4 5 6 7 8 9'.split(),columns='matching_id total_price avg_price currency ignored_products_count'.split())

# Przeliczenie na jedną walutę PLN
top_products['total_price'] = np.array(data['price']*data['quantity']*ratio)

# Przypisanie wartości do zestawienia końcowego 
top_products['matching_id'] = list(data['matching_id'])

# Przypisanie PLN
top_products['currency'] = 'PLN'
# Sortowanie danych według matching_id i total_price
top_products = top_products.sort_values(['matching_id','total_price'], ascending=[True, False])

# Usuwanie niepotrzebnych danych zgodnie z ilością top_priced_count
top_products = top_products.drop(top_products.index[n])
top_products = top_products.drop(top_products.index[n+n1+n2])

# Obliczenie średniej
def average(t):
    return sum(t) / len(t)

avg = average(([top_products['total_price'][0],top_products['total_price'][1]]))
avg1 = average(([top_products['total_price'][3],top_products['total_price'][2]]))
avg2 = average(([top_products['total_price'][6],top_products['total_price'][5], top_products['total_price'][4]]))    

# Stworzenie listy z wynikami końcowymi 
ins = [c-n, c-n, c1-n1, c1-n1, c2-n2, c2-n2, c2-n2]
out = [1, 2, 1 ,2 ,1, 2, 3]
out1 = ['1', '1', '2', '2', '3', '3', '3'] 
out2 = [avg, avg, avg1, avg1, avg2, avg2, avg2]
hier_index = list(zip(out, out1, out2, ins))
hier_index = pd.MultiIndex.from_tuples(hier_index)

top_products2 = pd.DataFrame(np.zeros((7,2)),index=hier_index,columns='total_price currency'.split())
top_products2.index.names = ['Num', 'matching_id', 'avg_price', 'ignored_products_count']

# Przypisanie PLN
top_products2['currency'] = 'PLN'
top_products2['total_price'] = np.array(top_products['total_price'])

top_products2.to_csv('top_products.csv')